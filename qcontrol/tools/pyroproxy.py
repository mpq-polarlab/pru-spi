# -*- coding: utf-8 -*-
"""
Created on Fri Oct 26 12:50:41 2012

@author: Christoph Gohle

This is an attempt to patch pyro, to allow complex data and classes to be passed
to the pyro clients as proxies so that they don't need to be pickled and that
property access and method calls remain happening on the server. This should vastly
simplify writing servers. Ideally client code just needs to operate on an initial
pyro client object instead of importing the object locally and nothing else needs
to be changed. Let's see how well this works.

The server code needs modification only in that far that data types and/or function
calls that should return proxies instead of local copies when called remotely
need to be labelled.
This is done by decorating function calls that should return proxies by the
'@returnpyroproxy' decorator. Objects that have an attribute _returnasproxy are
transformed into a proxy whenever they are returned by a remote function call
(no matter if there is a decorator or not). In fact the function decorator only
adds this attribute to the object being returned by the function call. The server
object has to be of type RemoteObjBase (a subclass of the Pyro.core.ObjBase). There
exist a helper function (start_server) that takes an Python object (module or class),
attaches a RemoteObjBase to it and starts a server thread. Of course it's also
possible to just subclass RemoteObjBase with your server code.

Whenever a proxy is to be returned by a remote call, it will be of type
RemoteProxyWithAttrs, which implements all the logic needed. As the name says,
it supports attribute access as well as some special functions (like __len__,
__dir__, __call__ etc) although this is not complete.

If the client calls a server method with a proxy as an argument, this argument
will be mapped to the server side object upon invocation of the server side method.
In that way even proxies can be used as if they were the real objects on the client
side during function calls (back into the same server).

The lifetime of the server side objects is determined by the references to the
proxies on the client side. Whenever a new proxy to a server side object is
returned in a remote call, the daemon keeps a reference to that object, connected
to the GUID stored in the client proxy. When a client side proxy object is garbage
collected, the reference that was held in the pyro daemon is released.

CAVEATS:

* Passing proxies out to other remote applications than the server where they originally
  came from is dangerous. If you don't keep a local reference, your
  local proxy is deleted, triggering deletion of the server side object and the sent
  over proxy object won't work anymore.
* modules can not be passed to the server (a workaround can be to pass the name
  of the module and import it on the server side. of course local data in the
  module is lost)
* most special functions don't work so far (like __mul__, __rmul__ etc)
* callbacks are currently not supported out of the box. Of course the client
  can always emulate callbacks by employing a server side polling method called
  periodically in a thread. It is also possible to start a server on the client
  side and pass a RemoteProxyWithAttrs instead of a callback function. This is
  not tested however and all the problems that are mentioned in the Pyro manual
  apply.
* I will find more while I try.
"""

import Pyro

import Pyro.core
import Pyro.naming
from threading import Thread
import time
import sys
import socket

proxydelid = '_pyroproxydel'
autodelete = '_autodelete'

from types import (UnboundMethodType, MethodType, BuiltinFunctionType,
                   BuiltinMethodType, FunctionType, StringType)
class RemoteObjBase(Pyro.core.ObjBase):
    def Pyro_dyncall(self, method, flags, args):
        #print 'Dyncall invoked with ', method, flags, args
        if method == proxydelid:
            #print 'calling destructor for %s'%self.GUID()
            self.getDaemon().disconnect(self.GUID())
            return
        if method == '__dir__':
            return dir(self.delegate if self.delegate is not None else self)
        else:
            #go through args and identifiy proxy objects
            indices = []
            for argno in xrange(len(args[0])):
                if hasattr(args[0][argno],'_invokePYRO'):
                    indices.append(argno)
            if len(indices)>0:
                pargs = []
                for i in xrange(len(args[0])):
                    if i in indices:
                        oid = args[0][i].objectID
                        pobj = self.getDaemon().getLocalObject(oid)
                        pargs.append(pobj.delegate or pobj)
                    else:
                        pargs.append(args[0][i])

            for argn, arg in args[1].iteritems():
                if hasattr(arg, '_invokePYRO'):
                    oid = arg.objectID
                    pobj = self.getDaemon().getLocalObject(oid)
                    args[1][argn] = pobj.delegate or pobj

            if len(indices)>0:
                args = (tuple(pargs), args[1])

            #print 'call original', method, flags, args
            result = self._patched_dyncall(method, flags, args)
            #print 'result = %s, %s, %s'%(result, isinstance(result, tuple), isinstance(result, list))
            if getattr(result, '_returnasproxy', False):
                result = self._create_autodeleteproxy(result)
            elif (isinstance(result, checktuple) or isinstance(result, checklist)): # and result[-1]=='_checkpyrotuple'):
                newresult = []
                #print 'checking...'
                for v in result:
                    if hasattr(v,'_returnasproxy'):
                        newresult.append(_create_autodeleteproxy(v, self.getDaemon()))
                    else:
                        newresult.append(v)
                result = type(result)(newresult)
            else:
                pass
            #print 'processed result is ', result
            return result

    def _patched_dyncall(self, method, flags, args):
        #there is a bug  in the original dyncall as in _r_ga and so forth
        #that's why I repeat the original Pyro_dyncall code here
        # update the timestamp
        self.lastUsed=time.time()
        # find the method in this object, and call it with the supplied args.
        keywords={}
        if flags & Pyro.constants.RIF_Keywords:
            # reconstruct the varargs from a tuple like
            #  (a,b,(va1,va2,va3...),{kw1:?,...})
            keywords=args[-1]
            args=args[:-1]
        if flags & Pyro.constants.RIF_Varargs:
            # reconstruct the varargs from a tuple like (a,b,(va1,va2,va3...))
            args=args[:-1]+args[-1]
        if keywords and type(keywords.iterkeys().next()) is unicode and sys.platform!="cli":
            # IronPython sends all strings as unicode, but apply() doesn't grok unicode keywords.
            # So we need to rebuild the keywords dict with str keys...
            keywords = dict([(str(k),v) for k,v in keywords.iteritems()])
        # If the method is part of ObjBase, never call the delegate object because
        # that object doesn't implement that method. If you don't check this,
        # remote attributes won't work with delegates for instance, because the
        # delegate object doesn't implement _r_xa. (remote_xxxattr)
        if method in dir(Pyro.core.ObjBase):
            return getattr(self,method) (*args,**keywords)
        else:
            # try..except to deal with obsoleted string exceptions (raise "blahblah")
            try :
                return getattr(self.delegate if self.delegate is not None else self,method) (*args,**keywords)
            except :
                exc_info = sys.exc_info()
                try:
                    if type(exc_info[0]) == StringType :
                        if exc_info[1] == None :
                            raise Exception, exc_info[0], exc_info[2]
                        else :
                            raise Exception, "%s: %s" % (exc_info[0], exc_info[1]), exc_info[2]
                    else :
                        raise
                finally:
                    del exc_info   # delete frame to allow proper GC

    # remote getattr/setattr support
    def _r_ha(self, attr):
        try:
            attr = getattr(self.delegate if self.delegate is not None else self,attr)
            if type(attr) in (UnboundMethodType, MethodType, BuiltinMethodType, FunctionType, BuiltinFunctionType):
                return 1 # method or function
        except:
            pass
        return 2 # attribute

    def _r_ga(self, attr):
        return getattr(self.delegate if self.delegate is not None else self, attr)

    def _r_sa(self, attr, value):
        setattr(self.delegate if self.delegate is not None else self, attr, value)

    def __str__(self):
        if self.delegate is not None:
            return self.__class__.__name__ +' for ' + str(self.delegate)
        else:
            return super(RemoteObjBase, self).__str__()

    def __repr__(self):
        if self.delegate is not None:
            return self.__class__.__name__ +' for ' + repr(self.delegate)
        else:
            return super(RemoteObjBase, self).__repr__()

    def _create_autodeleteproxy(self, obj):
            result = obj
            obj = RemoteObjBase()
            obj.delegateTo(result)
            #result._pyroobj = obj
            #print 'create proxy with python id %s and GUID %s'%(id(obj), obj.GUID())
            daemon = self.getDaemon()
            daemon.connect(obj)
            proxy = RemoteProxyWithAttrs(Pyro.core.PyroURI(daemon.hostname,
				obj.GUID(), prtcol=daemon.protocol, port=daemon.port) )
            #proxy = daemon.getAttrProxyForObj(obj)
            setattr(proxy, autodelete, False)
            return proxy


class RemoteProxyWithAttrs(Pyro.core.DynamicProxyWithAttrs):
    _local_attrs = Pyro.core.DynamicProxyWithAttrs._local_attrs+(autodelete,)

    def __del__(self):
        if getattr(self,autodelete,False):
            try:
                #print 'delete proxy with python id %s and GUID %s'%(id(self), self.objectID)
                self._invokePYRO(proxydelid, (), {}) #this one triggers attribute lookup, don't do it like this as
                                                        #this trigger remote attribute lookup, which does not exist
                #Pyro.core._RemoteMethod(self._invokePyro, proxydelid)()
            except Pyro.errors.ProtocolError:#,e:
                #I know this is an ugly hack, don't try this at home
                #print e
                pass
        super(RemoteProxyWithAttrs,self).__del__()

    def _invokePYRO(self, name, vargs = (), kargs = {}):
        #autogenerated proxies will have the attribute autodelete set to None
        #so that they don't trigger the autodelete mechanism when deleted on the
        #server side.
        #Not automatically generated proxies will not have the autodelete attribute
        result = super(RemoteProxyWithAttrs, self)._invokePYRO(name, vargs, kargs)
        if hasattr(result, autodelete):
            #print 'setting to autodelete.'
            #this is an autodelete proxy but we only delete the connection when
            #it's deleted on the client side
            result._autodelete = True
        return result

    def __getstate__(self):
        state = super(RemoteProxyWithAttrs, self).__getstate__()
        if hasattr(self, autodelete):
            state[autodelete] = getattr(self, autodelete)
        return state

    def __str__(self):
        return self._invokePYRO('__str__')

    def __repr__(self):
        return "<%s for %s>"%(self.__class__.__name__, self._invokePYRO('__repr__'))
        #return "%s(%s)"%(self.__class__.__name__, self.URI)

    def __getitem__(self, index):
        return self._invokePYRO('__getitem__',(index,))

    def __call__(self, *args, **kwargs):
        return self._invokePYRO('__call__', args, kwargs)

    #orgproxy_dir = Pyro.core.DynamicProxyWithAttrs.__dir__
    def __dir__(self):
        result = self.__dict__.keys()
        result.extend(dir(self.__class__))
        try:
            result.extend(self._invokePYRO('__dir__',tuple()))
        except:
            pass
        return result

    def __len__(self, *args, **kwargs):
        return self._invokePYRO('__len__', args, kwargs)

def returnpyroproxy(f):
    """use this decorator to achieve that the return type of the decorated function
    is a corresponding pyro proxy if called remotely"""
    def g(self, *args, **kwargs):
        result = f(self, *args, **kwargs)
        result._returnasproxy = True
        return result
    return g

class checktuple(tuple):
    pass

class checklist(list):
    pass

def returnpyroproxytuple(f):
    """if the return type of the decorated funciton is a list or tuple,
    this decorator achieves that the individual objects in it are being
    checked if they should be returned as proxies (have attribute '_returnasproxy')
    and a corresponding list or tuple is returned instead."""
    def g(self, *args, **kwargs):
        result = f(self, *args, **kwargs)
        if isinstance(result, tuple):
            return checktuple(result)
        elif isinstance(result, list):
            return checklist(result)
        return result
    return g

def _create_autodeleteproxy(obj, daemon):
        result = obj
        obj = Pyro.core.ObjBase()
        obj.delegateTo(result)
        result._pyroobj = obj
        #print 'create proxy with python id %s and GUID %s'%(id(obj), obj.GUID())
        daemon.connect(obj)
        proxy = daemon.getAttrProxyForObj(obj)
        setattr(proxy, autodelete, False)
        return proxy

class Server(Thread):
    """The server thread"""
    def __init__(self, daemon):
        Thread.__init__(self)
        self.pyroDaemon = daemon
        self.setDaemon(True) #allows the interpreter to exit even if this thread is still running

    #this is never being called when the thread is still running since the treading module keeps a reference to this thread.
    #so in the end it's actually useless... TODO: find a way around that
    def __del__(self):
        print 'del called'
        self.pyroDaemon.shutdown(True)
        self.join()

    def shutdown(self):
        """Shut down this server thread."""
        self.pyroDaemon.shutdown(True)

    def run(self):
        try:
            self.pyroDaemon.requestLoop()
        finally:
            print '... shutting down %s'%self.pyroDaemon
            self.pyroDaemon.shutdown(True)

def my_ip():
    """Weird hack to find proper local IP in the lab network.
    Somehow Python can't do this otherwise."""
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except socket.error:
        return None
    finally:
        s.close()
    return ip

def start_server(obj, name, ns_host = 'localhost', ns_port = 9090):
    """Start a Pyro server for obj and register it with the Nameserver using name.

    Ther server thread running the listener loop and the registered pyro object
    is returned. If name is None, an object without name is registered with the
    Pyro Daemon"""

    Pyro.config.PYRO_NS_HOSTNAME = ns_host
    Pyro.config.PYRO_NS_PORT = ns_port

    # connect to an existing nameserver
    ns = Pyro.naming.NameServerLocator().getNS()  # host = ns_host, port = ns_port, bcaddr = ns_bc_host)
    print 'name server found at %s' % ns.URI

    # start the pyro server
    print 'pyro daemon preinit'
    Pyro.core.initServer()

    daemon = Pyro.core.Daemon(host=my_ip())
    daemon.useNameServer(ns)

    print 'init %s',name
    pyroobj = RemoteObjBase()
    pyroobj.delegateTo(obj)
#    obj._pyroobj = pyroobj

    # connect
    print '%s running on %s'% (name, daemon.connectPersistent(pyroobj, name))

    print 'starting the server thread'
    serv = Server(daemon)
    serv.start()
    #obj._pyroserver = serv

    return serv, pyroobj

def get_client(name, ns_host = None, ns_port = None):
    """get a client proxy with attribute access for the server with name
    registered with the name server"""
    ns = Pyro.naming.NameServerLocator().getNS(ns_host, ns_port)
    uri = ns.resolve(name)
    client = RemoteProxyWithAttrs(uri)
    return client

def test_picam_client(name = 'picam'):
    cl = get_client(name)
    cl.Initialize()
    l = cl.getAvailableCameraIdList()
    cam = cl.Camera(l[0])
    cam.open()
    return cl, cam

if __name__ == '__main__':
    ns_host = 'localhost'
    ns_port = 9090

    class test1(object):
        test1 = 'test1'
        def prin(self):
            print('my string is "%s"'%self.test1)

        @property
        def test1property(self):
            return self.test1

        @test1property.setter #analysis:ignore
        def test1property(self, value):
            self.test1 = value

        def __del__(self):
            print 'deleting %s at %x'%(self.test1, id(self))

    class test2(object):
        test1 = 'test2'
        def prin(self):
            print('my string is "%s"'%self.test1)

        @property
        def test1property(self):
            return self.test1

        @test1property.setter #analysis:ignore
        def test1property(self, value):
            self.test1 = value

        @returnpyroproxy
        def gettest1(self):
            return test1()

        def __del__(self):
            print 'deleting %s at %x'%(self.test1, id(self))

    def testfunction(self):
        print 'local invocation'
        return 'callback return'


    if 'get_ipython' in globals():
        import Pyro.util
        def custom_tbhandler(self, etype, value, tb, tb_offset):
            Pyro.util.excepthook(etype, value, tb)
        get_ipython().set_custom_exc((Exception,), custom_tbhandler) #analysis:ignore
    else:
        sys.excepthook = Pyro.util.excepthook

    #test2 = main()
    # andorControl.SetTriggerMode(1)

