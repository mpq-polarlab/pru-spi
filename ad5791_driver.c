#include "pru_spi.h"

#define DRAM_START 0x200

//Definition of ramp
volatile uint8_t *channels = (uint8_t *)(DRAM_START); //Maximum 8 channels
volatile uint16_t *num_ramp_steps = (uint16_t *)(DRAM_START + 64);
volatile uint16_t *timestep = (uint16_t *)(DRAM_START + 80); //Maximum 8 ramps
volatile uint8_t *msg = (uint8_t *)(DRAM_START + 96);
//The message *msg is stored in (24-bit) big-endian, even though everything else 
//is little-endian, to match the way the AD5791 reads its input data.

void configure_dac(uint8_t channel) {
	/*Write command into the DAC's control register to put it into the right operational mode.
	See AD5791 manual for the meaning of the code.*/
	write_spi(channel, 0b001000000000000000110010, 24, 0, 1, 1);
}

void wait_for_trigger() {
	while(!(get_pin(BB_IOUPDATE))) {
		__delay_cycles(1);
	}
}

void wait_for_not_trigger() {
	while(get_pin(BB_IOUPDATE)) {
		__delay_cycles(1);
	}
}

int main(void) {
	uint16_t i;
	uint8_t ramp_index;
	uint32_t step_ctr = 0;
	uint32_t max_steps_current = 0;
	uint32_t register_prefix = 0b0001 << 20;
	uint32_t target_voltage; //Only 20 bit of this will be used!
	uint8_t num_channels;
	uint8_t read_from_sram = 0;

	//Set all output ports to starting values
	for(i=NOT_EXT_IOUPDEN; i<=BB_SCLK; i++) {
		set_pin(i, 0);
	}
	set_pin(BB_SCLK, 1); //To match clock polarity
	set_pin(BB_CS_P0SPI, 1); //Connection to back plane should be closed by default
	for(i=0; i<32; i++) {
		configure_dac(i);
	} //Just configuring all the DACs is easier than to figure out which ones are needed.
	wait_for_trigger(); //Need to make sure that a trigger comes, otherwise...
	wait_for_not_trigger(); //...the DACs will remain un-updated after the initialization.

	for (ramp_index=0; ramp_index<8; ramp_index++) {
		num_channels = 0;
		for (i=0; i<8; i++) {
			if (channels[8*ramp_index + i] < 32) {
				num_channels++;
			}
		}
		wait_for_trigger();
		set_pin(NOT_EXT_IOUPDEN, 1); //Do not listen to external triggers during ramp.
		max_steps_current += num_ramp_steps[ramp_index] * num_channels;
		while (step_ctr<max_steps_current) {
			for (i=0; i<num_channels; i++) {
				target_voltage = (((uint32_t)(msg[3*step_ctr]<<16))
						| ((uint32_t)(msg[1 + 3*step_ctr]<<8))
						| ((uint32_t)(msg[2 + 3*step_ctr])));
				write_spi(channels[8*ramp_index + i], register_prefix | target_voltage, 24, 0, 1, 1);
				step_ctr++;
				if (step_ctr >= ((0x4000 - DRAM_START - 96) / 3) && !(read_from_sram)) {
					//Once we have arrived at the end of DRAM, continue with SRAM.
					msg = (uint8_t *)(0x10000 - 3*step_ctr);
					read_from_sram = 1;
				}
			}
			// Additional delay is needed such that no DAC can get its ~LDAC taken low
			// *before* ~SYNC goes high. (See AD5791 manual, p.20). This can otherwise happen
			// due to different delays on the DAC board.
			__delay_cycles(100);
			for (i=0; i<timestep[ramp_index]; i++) {
				__delay_cycles(20); //Corresponds to 0.15 us
			}
			set_pin(NOT_EXT_IOUPDEN, 0); //IO_UPDATE pulse after all channels are programmed
			set_pin(NOT_EXT_IOUPDEN, 1);
		}
		set_pin(NOT_EXT_IOUPDEN, 0);
		wait_for_not_trigger();
	}
	return 1;
}
