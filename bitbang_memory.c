#include "pru-spi.h"

volatile uint8_t *memory = (uint8_t *) 0x200;
int i, j;

int main(void) {
	set_pin(NOT_EXT_IOUPDEN, 0);
	while(!(get_pin(BB_IOUPDATE))) {
		__delay_cycles(1);
	}

	for (i=0; i<3000; i++) {
		write_spi(0, memory[i], 8, 0, 0, 0);
	}
}
