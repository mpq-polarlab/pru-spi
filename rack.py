# -*- coding: utf-8 -*-
"""
@author: Roman Bause

Here, the devices in the rack and their addresses are defined. Whenever the hardware
is changed, this file needs to be changed accordingly.

The communication via Pyro follows the way it was written previously by 
Christoph Gohle.

"""

import beaglebone_server
import socket
import time
from qcontrol.tools import pyroproxy
import Pyro
from Pyro.naming import NameServerLocator
from Pyro.errors import NamingError

pyroid = 'dacbeagle2'
nsip = None
nsport = None
nsident = None
nsbcaddr = None
hostname = socket.gethostname()
myip = socket.gethostbyname(hostname)
Pyro.config.PYRO_HOST = myip

#########################
# put your rack configuration here. These are the SPI addresses of AD5791 devices in the rack.
rack_configuration = [1, 2, 4, 8]
board = beaglebone_server.Beaglebone_PRU(rack_configuration)
#########################
 
def getNs():
    locator = NameServerLocator(identification=nsident)
    NS = locator.getNS(nsip, nsport, 1, bcaddr=nsbcaddr)
    NS._setIdentification(nsident)
    return NS

serv, pyroobj = None, None
if __name__ == '__main__':
    locator = NameServerLocator(identification=nsident)
    ns = None
    while True:
        time.sleep(1)
        try:
            if serv is None or serv.is_alive()==False:
                print 'Server dead (or new). start a fresh one'
                serv, pyroobj = pyroproxy.start_server(board, pyroid, nsip)
                print('Server started successfully: %s, %s' % (repr(serv), repr(pyroobj)))
            #check that we are still registered with the name server
            try:
                if ns is None:
                    ns = getNs()
                    print('Nameserver found: %s' % repr(ns))
                namelist = ns.list(None)
                if (pyroid,1) in namelist:
                    pass
                else:
                    print 'reconnect %s running on %s'% (pyroid, 
                        serv.pyroDaemon.connectPersistent(pyroobj, pyroid))
            except NamingError:
                #try to reconnect to NS
                print 'connetion to name server lost. try to reconnect'
                ns = getNs()
        except Exception as e:
            from traceback import format_exc
            tb = format_exc()
            print '%s\n WARNING: server loop failed %s'%(tb, e)
