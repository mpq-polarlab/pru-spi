#!/bin/bash

machine=$(awk '{print $NF}' /proc/device-tree/model)
echo -n $machine
if [ $machine = "Black" ]; then
    echo " Found"
    out_pins="P8_15 P8_16 P8_18 P9_11 P9_12 P9_13 P9_14 P9_15 P9_17 P9_18 P9_22"
else
    echo " Not Found"
    out_pins=""
fi

if [ $machine = "Black" ]; then
    echo " Found"
    in_pins="P8_17 P9_21"
else
    echo " Not Found"
    in_pins=""
fi

for pin in $out_pins
do
    echo $pin
    config-pin $pin gpio
    config-pin $pin out
    config-pin -q $pin
done

for pin in $in_pins
do
    echo $pin
    config-pin $pin gpio
    config-pin $pin in
    config-pin -q $pin
done
