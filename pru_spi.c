#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"
#include "pru_spi.h"

#define SLOWDOWN 0 //Introduces waiting after every clock cyle in case the programming
//is too fast due to delays or parasitic capacitance. Larger values mean slower clock.
//On test system, a value of 0 resulted in 1 MHz clock, while 1000 resulted in 100 kHz.
//It should behave linearly.

//Port addresses of GPIO ports, see Table 2-2 in the ARM335x Reference manual
volatile uint32_t *gpio0 = (uint32_t *)0x44e07000;
volatile uint32_t *gpio1 = (uint32_t *)0x4804c000;
volatile uint32_t *gpio2 = (uint32_t *)0x481ac000;

void set_pin(Pin pin, uint32_t val) {
	/* Set pin to val, i.e. if val==1, pin will be set to logic high,
	 * if val==0, it will be set to logic low. */
	if (val) {
		val = (GPIO_SETDATAOUT/4);
	} else {
		val = (GPIO_CLEARDATAOUT/4);
	}
	switch(pin) { //See Beaglebone PRU output header tables.
		case NOT_EXT_IOUPDEN: gpio1[val] = (0x1<<14); break;
		case NOT_EXT_RSTEN: gpio1[val] = (0x1<<15); break;
		case BB_RESET: gpio2[val] = (0x1<<1); break;
		case BB_CS_P1: gpio0[val] = (0x1<<30); break;
		case BB_CS_P2: gpio1[val] = (0x1<<28); break;
		case BB_CS_P3: gpio0[val] = (0x1<<31); break;
		case BB_CS_P4: gpio1[val] = (0x1<<18); break;
		case BB_CS_P0: gpio1[val] = (0x1<<16); break;
		case BB_CS_P0SPI: gpio0[val] = (0x1<<5); break;
		case BB_SDO: gpio0[val] = (0x1<<4); break;
		case BB_SCLK: gpio0[val] = (0x1<<2); break;
	}
}

uint32_t get_pin(Pin pin) {
	/* Read out value of an input pin */
	switch(pin) {
		case BB_SDI: return gpio0[GPIO_DATAIN/4]&(0x1<<3);
		case BB_IOUPDATE: return gpio0[GPIO_DATAIN/4]&(0x1<<27);
	}
	return -1; //In case of error
}

void write_spi(const uint8_t addr, const uint32_t msg, const uint8_t msg_len,
	const uint8_t lsb_first, const uint8_t cpol, const uint8_t cpha) {
	/*
	* Write out a given string of bits by bit-banging on the SPI bus.
	* addr is the address of the destination, this controls the values of the CS ("channel select") bits.
	* msg is the message to be sent.
	* msg_len is the message length in bits (maximum 32).
	* lsb_first decides for MSB or LSB first transfer method.
	* cpol and cpha decide which SPI operation mode to use. (e.g. data write on clock rising or falling edge)
	* It is assumed that SCLK is already in the correct state at the beginning (low for cpol==0, high for cpol==1)
	*/
	uint8_t i;
	set_pin(BB_CS_P0, addr & 0x1);
	set_pin(BB_CS_P1, addr & (0x1<<1));
	set_pin(BB_CS_P2, addr & (0x1<<2));
	set_pin(BB_CS_P3, addr & (0x1<<3));
	set_pin(BB_CS_P4, addr & (0x1<<4));
	set_pin(BB_CS_P0SPI, 0); //Open the signal path to the back plane.
	// Start message transmission
	if (cpol) {
		for (i=0; i<msg_len; i++) {
			if (cpha) {
				set_pin(BB_SCLK, 0);
			}
			if (lsb_first) {
				set_pin(BB_SDO, (msg >> i) & 1);
			} else {
				set_pin(BB_SDO, ((msg << i) & (1 << msg_len-1)));
			}
			if (cpha) {
				__delay_cycles(SLOWDOWN);
				set_pin(BB_SCLK, 1);
				__delay_cycles(SLOWDOWN);
			} else {
				__delay_cycles(SLOWDOWN);
				set_pin(BB_SCLK, 0);
				__delay_cycles(SLOWDOWN);
				set_pin(BB_SCLK, 1);
			}
		}
	} else {
		for (i=0; i<msg_len; i++) {
			if (cpha) {
				set_pin(BB_SCLK, 1);
			}
			if (lsb_first) {
				set_pin(BB_SDO, (msg >> i) & 1);
			} else {
				set_pin(BB_SDO, ((msg << i) & (1 << msg_len-1)));
			}
			if (cpha) {
				__delay_cycles(SLOWDOWN);
				set_pin(BB_SCLK, 0);
				__delay_cycles(SLOWDOWN);
			} else {
				__delay_cycles(SLOWDOWN);
				set_pin(BB_SCLK, 1);
				__delay_cycles(SLOWDOWN);
				set_pin(BB_SCLK, 0);
			}
		}
	}
	set_pin(BB_SDO, 0); //Make sure it is set to 0 in the end (Not actually needed, but convenient for debugging).
	set_pin(BB_CS_P0SPI, 1); //All CS bits will be pulled up, so no chips are addressed.
}
