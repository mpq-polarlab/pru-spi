import numpy as np
import subprocess
import mmap

# Global constants. These are specific to the Beaglebone hardware and / or the 
# PRU program that performs communication via SPI bus. Do not change them unless
# you know what you are doing.
max_devices = 8
max_ramps = 8
v_min = -5. #volt
v_max = 5. #volt
v_step = (v_max - v_min) / (2**20 - 1) #Difference between two neighboring voltages
available_pru_dram_size = 5258 # in units of 24-bit words
available_pru_sram_size = 4096 # in units of 24-bit words

# cycle_duration must be calibrated whenever SLOWDOWN is changed in the
# driver. To be sure, also do it when using a different
# Beaglebone, to get reliable ramp timing. wait_time_step
# only depends on the PRU clock speed, so it should never have to be changed.
cycle_duration = 13.6e-6 # seconds
wait_time_step = 0.151e-6 # seconds

class Beaglebone_PRU:
    '''Manages Ramps and how they are written into memory accessible by the PRU,
    which then performs SPI communication with the AD5791 DAC.

    add_ramp()
    Create a new ramp with the following parameters:
    -addresses: List of integers. Which devices will ramp. All other devices will keep previous value.
    -ramp_duration: float. Duration of the whole ramp. In units of seconds.
    -initial_values: List of floats. Initial voltage for each channel.
    -final_values: List of floats. Final voltage for each channel.
    -num_steps: Integer. How many steps the ramp contains.
    -ramp_index: Integer. There are eight spots for ramps, every new instance will be put
    into one of these spots. If nothing is specified, it will be put into the first free spot.

    write_ramps_to_pru()
    Write data for all ramps into PRU-readable memory. Must be called before any ramps can happen.
    The memory layout is hard-coded to perfectly match the one coded into the DAC driver.
    Changing max_devices or max_ramps will require a re-design of this memory layout!
    '''

    _returnasproxy = True

    def __init__(self, addresses):
        self.addresses = addresses
        self.ramps = [None] * max_ramps

    def add_ramp(self, addresses, ramp_duration, initial_values, final_values,
                 num_steps, ramp_index=None):
        if len(addresses) > max_devices:
            raise ValueError('Too many addresses.')
        for address in addresses:
            if address not in self.addresses:
                raise ValueError("%s %i" %
                    ("This Beaglebone does not have a device with specified address", address))
        if (len(addresses) != len(initial_values)) or (len(addresses) != len(final_values)):
            raise ValueError("Number of initial or final values does not match number of addresses.")
        if (max(max(initial_values), max(final_values)) > v_max
            or min(min(initial_values), min(final_values)) < v_min):
            raise ValueError("Target voltage out of range.")
        if ramp_index:
            if ramp_index < max_ramps:
                self.ramps[ramp_index] = Ramp_AD5791(addresses, ramp_duration, initial_values, 
                                              final_values, num_steps)
            else:
                raise IndexError
        else:
            try: # If no ramp index is given, put the ramp at the first free spot.
                self.ramps[next(i for i, x in enumerate(self.ramps) if x is None)] = \
                    Ramp_AD5791(addresses, ramp_duration, initial_values, final_values, num_steps)
            except StopIteration:
                raise IndexError

    def delete_ramps(self):
        self.ramps = [None] * max_ramps

    def _encode_dac_input(self, v):
        '''Turn a numpy array of target voltages into binary strings to be written into the DAC
        input register. Output is an array of non-separated, signed 24-bit integer, MSB first,
        offset-encoded bits in the form of a string.'''
        result = ''
        v = np.round((v - v_min) / v_step).astype(int)
        for i in np.nditer(v):
            # This one is big-endian to match with the way the AD5791 reads its data in.
            result += (to_bytes(i, 3, endianness='big'))
        return result
    
    def write_ramps_to_pru(self):
        '''Write ramps into PRU memory. Some trickery goes on here to use all of the 28 kiB
        of memory available. PRU0 is used for the ramping. The first part of the ramps
        is written into PRU0's DRAM (addresses 0x0200 to 0x2000). (The first 512 bytes are
        reserved for stack and heap of the progam itself.) It then directly continues into PRU1's
        DRAM (addresses 0x2000 to 0x4000). This will only work if PRU1 is not used!
        After this, it continues into the SRAM (adresses 0x10000 to 0x13000). The jump here is
        synchronized with the DAC driver.
        '''
        pru_memory_len = 0x00080000
        pru_addr_offset = 0x4A300000
        pru0_dram_offset = 0x00000000
        pru_sram_offset = 0x00010000
        pru_heap_offset = 0x00000200
        write_to_dram = False

        self._get_num_steps_total()
        with open('/dev/mem', 'r+') as f:
            pru_memory = mmap.mmap(f.fileno(), pru_memory_len, offset=pru_addr_offset)
        pru_memory.seek(pru0_dram_offset + pru_heap_offset)
        for r in self.ramps:
            for i in range(max_devices):
                if r and i < len(r.addresses):
                    pru_memory.write_byte(chr(r.addresses[i]))
                else:
                    pru_memory.write_byte(chr(0xff)) # No SPI device can have this address.
        for r in self.ramps:
            if r:
                pru_memory.write(to_bytes(r.num_steps, 2, endianness='little'))
            else:
                pru_memory.write(to_bytes(0, 2, endianness='little'))
        for r in self.ramps:
            if r:
                pru_memory.write(to_bytes(r._timestep, 2, endianness='little'))
            else:
                pru_memory.write(to_bytes(0, 2, endianness='little'))
        for r in self.ramps:
            if r:
                encoded_dac_input = self._encode_dac_input(r._get_table())
                remaining_memory = (0x4000 - pru_memory.tell())
                if (not write_to_dram) and (remaining_memory < len(encoded_dac_input)):
                    encoded_dac_input_dram = encoded_dac_input[:(remaining_memory//3)*3]
                    encoded_dac_input_sram = encoded_dac_input[(remaining_memory//3)*3:]
                    pru_memory.write(encoded_dac_input_dram)
                    pru_memory.seek(pru_sram_offset)
                    write_to_dram = True
                    pru_memory.write(encoded_dac_input_sram)
                else:
               	    pru_memory.write(encoded_dac_input)
        pru_memory.close()
        
    def start_pru(self):
        p = subprocess.Popen('make', stdout=subprocess.PIPE)
        p.wait()
        self.write_ramps_to_pru()
        self.delete_ramps()
        
    def _get_num_steps_total(self):
        existing_ramps = [r for r in self.ramps if r is not None]
        result = 0
        for r in existing_ramps:
            result += r.num_steps * len(r.addresses)
        if result > (available_pru_dram_size + available_pru_sram_size):
            raise RuntimeError("Too many steps, ramp does not fit in PRU memory.")
        return result
    
def to_bytes(n, length, endianness='little'):
    h = '%x' % n
    s = ('0'*(len(h) % 2) + h).zfill(length*2).decode('hex')
    return s if endianness=='big' else s[::-1]

class Ramp_AD5791:
    def __init__(self, addresses, ramp_duration, initial_values, final_values, num_steps):
        self.addresses = addresses
        self.num_steps = num_steps
        self.ramp_duration = ramp_duration
        self._timestep = self._get_timestep()
        self.v_initial = initial_values
        self.v_final = final_values
        
    def _get_timestep(self):
        wait_time = (self.ramp_duration / self.num_steps
                     - cycle_duration * len(self.addresses))
        if wait_time < 0:
            raise ValueError("Ramp duration too short, can not ramp so fast.")
        timestep = np.round(wait_time / wait_time_step).astype(int)
        if timestep > 65535:
            raise ValueError("Ramp duration too long, can not ramp so slowly.")
        return timestep

    def _get_table(self):
        '''Return a numpy array of dimension n*m, where n is the
        number of addresses and m is the number of ramp steps.
        Each element corresponds to the target voltage for this address at this ramp step.
        '''
        result = np.zeros((self.num_steps, len(self.addresses)))
        for i, address in enumerate(self.addresses):
            # Workaround: By default, linspace would make the initial value part of the ramp
            # but not the final value. To get expected behaviour, I cut the last element off.
            result[:, i] = np.linspace(self.v_initial[i], self.v_final[i], self.num_steps+1)[1:]
        return result
        
