This is the fast driver for the AD5791 DAC. It uses the PRU chip on a 
Beaglebone to communicate much faster and with better timing precision 
than can be done from a Linux-based system. Contains a fully functional
SPI communication program for the PRU, which can send SPI commands at up to 1
MHz clock speed.

Author: Roman Bause, 2020

List of files:

AM335x_PRU.cmd: Linker command file for the PRU. Required for compilation of C 
programs to run on the PRU. Found in the PRU cookbook by Mark A. Yoder.

Make-bitbang-memory: Makefile for the C program bitbang-memory.c Usage:
make -f Make-bitbang-memory. This compiles it, moves the binary to the PRU
and starts it.

Makefile: Makefile for dac-driver.c Usage: make. This compiles it, moves it to the PRU
and starts it.

bitbang-memory.c: Little program for debugging purposes only. Runs on PRU. You can speficy a 
memory adress, then this program will just use SPI to bitbang out the content of memory
beginning at this address. A scope can then be used to read the SPI commands. This
is currently the only way to know what the PRU read from memory.

ad5791-driver.c: The heart of the program. Runs on PRU. Takes definitions of ramps 
from PRU-readable memory and calls appropriate SPI communication to make ramps 
on the AD5791 happen. Also manages triggers, etc.

beaglebone-server.py: Runs on Beagebone CPU. Handles communication with experiment control. 
Recieves ramping parameters via Pyro, performs translation into PRU-understandable 
data structure and writes it into the right part of memory. This contains calibration 
parameters for ramp duration.

gpio-setup.sh: Sets output and input pins of the Beaglebone into the correct modes
so they can send/recieve data from/to PRU. This must be run after Beaglebone startup
once, otherwise no communcation with the DAC will be possible.

pru-spi.c: General purpose SPI for the PUR. Supports all SPI modes (CPOL 0/1, CPHA 0/1),
word lengths  up to 32, as well as different clock speeds (by changing the 
preprocessor macro  SLOWDOWN). Can be reused for anything that requires SPI communcation 
in principle,  though it was only tested with the AD5791. On the testing Beaglebone 
system, 1 MHz  clock speed, timing jitter < 200ns and < 1us trigger reponse 
time were measured.

pru-spi.h: Header file for pru-spi.c.

resource_table_empty.h: Needed for compilation of C programs for PRU. 
Taken from PRU cookbook.

