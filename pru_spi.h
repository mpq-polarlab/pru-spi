#ifndef PRU_SPI_H
#define PRU_SPI_H

#include <stdint.h>

//Constants for changing or reading ports
#define GPIO_CLEARDATAOUT 0x190
#define GPIO_SETDATAOUT 0x194
#define GPIO_DATAIN 0x138

//Pins are named in the same way as the connections on the Beaglebone board they connect to.
typedef enum {
	BB_SDI,
	BB_IOUPDATE,
	NOT_EXT_IOUPDEN,
	NOT_EXT_RSTEN,
	BB_RESET,
	BB_CS_P1,
	BB_CS_P2,
	BB_CS_P3,
	BB_CS_P4,
	BB_CS_P0,
	BB_CS_P0SPI,
	BB_SDO,
	BB_SCLK,
} Pin;

void set_pin(Pin pin, uint32_t val);

uint32_t get_pin(Pin pin);

void write_spi(const uint8_t addr, const uint32_t msg, const uint8_t msg_len,
	const uint8_t lsb_first, const uint8_t cpol, const uint8_t cpha);

#endif /*PRU_SPI_H*/
